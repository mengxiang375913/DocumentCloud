��          �      �       0     1     @  O   P     �     �     �  i   �     4  Z   =     �     �  ,   �  k  �     W     ^  9   k     �     �     �  ?   �       N        h     u     �                	                             
             Authentication Change password Controls the mechanism used to authenticated user. Options are: username, email Current user password change Email Logout Maximum time an user clicking the "Remember me" checkbox will remain logged in. Value is time in seconds. Password Please enter a correct email and password. Note that the password field is case-sensitive. Remember me This account is inactive. Your password has been successfully changed. Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-07-15 03:48+0000
Last-Translator: Roberto Rosario
Language-Team: Chinese (China) (http://www.transifex.com/rosarior/mayan-edms/language/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
 鉴权 修改密码 控制鉴权的方式，可选项为：用户名，密码 当前用户密码修改 电子邮件 退出 【记住密码】功能的逾期时间。时间单位：秒。 密码 请输入正确的邮箱或者密码。注意！密码是大小写敏感的。 记住密码 此账号未激活 你的密码已经修改成功 
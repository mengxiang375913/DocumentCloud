��    C      4  Y   L      �      �      �  (   �  )     )   F      p  
   �     �  	   �     �     �     �     �  #   �          !  	   /     9     >     L  (   f     �     �  N   �     �          +     ;     B     G     P     W     h     y          �     �     �     �     �  	   �     �     �     �  '   �  (   	     C	  =   [	     �	     �	     �	  	   �	     �	     �	     �	     �	     
     
     
  U    
  ?   v
     �
  !   �
     �
     �
     �
  k       x     �  2   �  2   �  2        J     h     u     |     �     �     �     �     �     �     �     �     
               .     M     `  N   s     �      �     �     	       	        !     (     8     E     L  	   S     ]     j     q     x     �     �     �     �     �     �     �          !     (     /     A     H  	   O     Y     o     �     �     �     �  ,   �     �     �     �     �          .       C      '   	       /   %      2   A          )       +   ?         ,                         6   9   !      8   4   >         5           B       1          0                             &   ;   -   3      (   "   :   *                                          #         7   =   
   $   <       @                     %(object)s created successfully. %(object)s deleted successfully. %(object)s not created, error: %(error)s %(object)s not deleted, error: %(error)s. %(object)s not updated, error: %(error)s. %(object)s updated successfully. About this Add Anonymous Available attributes: 
 Check for updates Common Current user details Current user locale profile details Data filters Date and time Date time Days Documentation Edit current user details Edit current user locale profile details Edit details Edit locale profile Enter a valid 'internal name' consisting of letters, numbers, and underscores. Error log entries Error log entries for: %s Error log entry Errors File Filename Filter Filter not found Filter selection Forum Hours License Locale profile Main Minutes Must select at least one item. Namespace No action selected. None Object Operation performed on %(count)d object Operation performed on %(count)d objects Other packages licenses Path to the logfile that will track errors during production. Remove Result Results for filter: %s Selection Setup Setup items Shared uploaded file Shared uploaded files Source code Support System Temporary directory used site wide to store thumbnails, previews and temporary files. The version you are using is outdated. The latest version is %s Tools Unable to transfer selection: %s. User User details Your version is up-to-date. Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-08-27 16:48+0000
Last-Translator: Roberto Rosario
Language-Team: Chinese (China) (http://www.transifex.com/rosarior/mayan-edms/language/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
 %(object)s 已成功创建 %(object)s 已成功删除。 %(object)s 未被创建，错误信息：%(error)s %(object)s 未被删除，错误信息：%(error)s %(object)s 未被更新，错误信息：%(error)s %(object)s 已成功更新。 关于此项 新增 匿名用户 可用属性：
 检查更新 通用 当前用户详情 当前用户地域设定详情 数据过滤 日期和时间 日期时间 天 文档 编辑当前用户详情 编辑当前用户地域详情 编辑用户详情 编辑地域设置 请输入一个仅由字母、数字和下划线组成的“内部名称”。 错误日志入口 错误日志入口，针对：%s 错误日志入口 错误 文件 文件名 过滤 过滤未找到 过滤选项 论坛 小时 许可证 地域设置 首要 分钟 至少需要选择一项 命名空间 请选择一个操作 无 对象 已处理 %(count)d 个对象 已处理 %(count)d 个对象 其它 过程错误日志存储地址 移除 结果 过滤结果：%s 选择 设置 设置项 上传的分享文件 上传的分享文件 源码 支持 系统 时区 你的版本已过期，最新版本为：%s 工具 无法传输所选：%s。 用户 用户详情 你的版本已是最新。 
��    2      �  C   <      H  ;   I     �     �     �     �     �     �       "        >     U     h          �  "   �  9   �               )     5     J     ^  	   u       9   �     �  &   �  '   �     $  !   B     d     x     �     �     �  "   �     �       %     &   @     g     �     �     �     �     �     �     �  V   �  k  *	  $   �
     �
     �
     �
     �
     �
          !     7     R     _     l     }     �     �  '   �     �     �     �                     0     7  !   G     i     p     �     �     �     �     �     �     �     �               +     ;     X     s     �     �     �     �     �     �     �  G   �     !       '   1                       $             -   +          .       
         ,   "           (            0                	   2            /         )   %                 &          *              #                                Amount of time to hold the document checked out in minutes. Block new version upload Check expired checkouts Check in document Check in documents Check in the document: %s? Check in/out Check out date and time Check out details for document: %s Check out details view Check out document Check out document: %s Check out documents Check out expiration Check out expiration date and time Check out expiration date and time must be in the future. Check out time Checked in/available Checked out Checkedout documents Checkout expiration Checkout time and date Checkouts Checkouts periodic Do not allow new version of this document to be uploaded. Document Document "%s" checked in successfully. Document "%s" checked out successfully. Document already checked out. Document automatically checked in Document checked in Document checked out Document checkout Document checkouts Document forcefully checked in Document has not been checked out. Document status Documents checked out Error trying to check in document; %s Error trying to check out document; %s Forcefully check in documents New version block New version blocks New versions allowed? No Period User Yes You didn't originally checked out this document. Forcefully check in the document: %s? Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-06-23 05:43+0000
Last-Translator: Roberto Rosario
Language-Team: Chinese (China) (http://www.transifex.com/rosarior/mayan-edms/language/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
 持有文档检出的时间分钟数 禁止上传新版本 检查逾期检出 签入文档 签入文档 签入文档：%s？ 签入/检出 检出日期和时间 文档：%s的检出信息 检出详情 检出文档 检出文档: %s 检出文档 检出逾期 检出过期的日期和时间 检出过期时间必须晚于当前。 检出时间 签入/可选 检出 检出文档 检出逾期 检出时间和日期 检出 周期性检出 此文档不允许上传新版本 文档 文档"%s"签入成功 文档"%s"检出成功 文档已经检出 文档自动签入 文档签入 文档检出 文档检出 文档检出 文档强制签入 文档未被检出 文档状态 检出的文档 尝试签入文档出错: %s 尝试检出文档出错%s 强制文档签入 禁止新版本 禁止新版本 允许新建版本？ 否 周期 用户 是 你并不是文档的原始检出者，要强制签入文档: %s 吗？ 
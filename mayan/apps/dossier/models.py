# coding=utf-8
from __future__ import unicode_literals

import uuid

from django.conf import settings
from django.db import models


# Create your models here.

class Dossier(models.Model):
    uid = models.CharField(default=str(uuid.uuid4()).replace("-", ""),verbose_name="档案")
    title = models.CharField(max_length=20,verbose_name="标题")
    parts_number = models.CharField(max_length=40,verbose_name="件号")
    reference_number = models.CharField(max_length=40,verbose_name="文号")
    year = models.IntegerField(verbose_name="年度")
    detail = models.CharField(max_length=40,verbose_name="细节")
    # explorer_id = models.ForeignKey()
    manager = models.ForeignKey(settings.AUTH_USER_MODEL)
    # department_id = models.ForeignKey(verbose_name="部门")
    
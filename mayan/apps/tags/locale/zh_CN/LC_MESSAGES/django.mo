��    9      �  O   �      �  w   �     a     e  
   l     w  D   �     �     �     �  Q        U     d  
   t          �     �  2   �  6   �  1        =     J  	   X     b     }     �  	   �  '   �     �     �  #   �     �  
   �               /  D   J     �  ?   �  @   �     	  2   3	  3   f	     �	  -   �	  .   �	  2   
  3   C
     w
     �
     �
     �
     �
     �
      �
  	     #     k  C  F   �     �     �            #        B     ]     s  H   z     �     �     �     �     �     	       1   ,  5   ^     �     �     �     �     �     �     �  &   �          $     +     J     Q     ^      k     �  #   �     �  2   �  2        6  1   O  1   �     �  /   �  /   �  5   ,  5   b     �     �     �     �     �               7     D     6   5       /       -   !   $             &   *          
           0       (   %   1   	      "           3      7   8   +   #                         .                        )   2                             4   9            ,                       '                               API URL pointing to a tag in relation to the document attached to it. This URL is different than the canonical tag URL. All Attach Attach tag Attach tags Attach tags to %(count)d document Attach tags to %(count)d documents Attach tags to document: %s Attach tags to documents Color Comma separated list of document primary keys to which this tag will be attached. Create new tag Create new tags Create tag Delete Delete tag: %s Delete tags Delete the selected tag? Delete the selected tags? Document "%(document)s" is already tagged as "%(tag)s" Document "%(document)s" wasn't tagged as "%(tag)s Document tag Document tags Documents Documents with the tag: %s Edit Edit tag: %s Edit tags Error deleting tag "%(tag)s": %(error)s Label Preview Primary key of the tag to be added. Remove Remove tag Remove tags Remove tags from document: %s Remove tags from documents Remove tags to %(count)d document Remove tags to %(count)d documents Tag Tag "%(tag)s" attached successfully to document "%(document)s". Tag "%(tag)s" removed successfully from document "%(document)s". Tag "%s" deleted successfully. Tag attach request performed on %(count)d document Tag attach request performed on %(count)d documents Tag attached to document Tag delete request performed on %(count)d tag Tag delete request performed on %(count)d tags Tag remove request performed on %(count)d document Tag remove request performed on %(count)d documents Tag removed from document Tags Tags for document: %s Tags to attach to the document Tags to be attached. Tags to be removed. Tags to remove from the document View tags Will be removed from all documents. Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-08-27 16:33+0000
Last-Translator: Roberto Rosario
Language-Team: Chinese (China) (http://www.transifex.com/rosarior/mayan-edms/language/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
 指代标签和文档关系的API URL，与标准的标签URL不同。 全部 添加 添加标签 添加标签 为 %(count)d 份文档添加标签 添加标签到文档：%s 给文档添加标签 颜色 指定需要添加此标签的文档主键，多项则以逗号分隔。 创建新标签 创建新标签 创建标签 删除 删除标签：%s 删除标签 删除所选标签？ 文档 "%(document)s"已经被标记为 "%(tag)s" 文档 "%(document)s" 并未加上标签 "%(tag)s 。 文档标签 文档标签 文档 文档，带有标签：%s 编辑 编辑标签：%s 编辑标签 删除标签"%(tag)s"失败: %(error)s 标签 预览 将要添加的标签的主键 移除 移除标签 移除标签 移除标签，针对文档：%s 从文档中移除标签 为 %(count)d 份文档移除标签 标签 文档"%(document)s"添加标签"%(tag)s"成功。 标签"%(tag)s"成功从文档"%(document)s"移除 标签 "%s" 删除成功 %(count)d 份文档的标签添加请求已完成 %(count)d 份文档的标签添加请求已完成 添加到文档的标签 已执行对 %(count)d 个标签的删除请求 已执行对 %(count)d 个标签的删除请求 已执行对 %(count)d 个文档的标签移除请求 已执行对 %(count)d 个文档的标签移除请求 从文档移除的标签 标签 标签，用于文档：%s 要添加到文档的标签 将要添加的标签 将要移除的标签 要从文档移除的标签 查看标签 将从所有文档中移除 
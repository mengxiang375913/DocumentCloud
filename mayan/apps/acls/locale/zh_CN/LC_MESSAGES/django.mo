��    $      <  5   \      0     1  �   6  I   �          -     J     Y     f  U   |     �     �  8   �  	   !     +     8     L     h      p     �     �  G   �  	   �     �       A     K   Y  F   �  D   �     1     ?  -   D     r  $   x  9   �  	   �  k  �     M	  ^   `	  (   �	     �	     �	     
     '
     4
  9   A
     {
     �
  3   �
     �
     �
     �
     
       "   5     X     j  !   n     �     �     �  U   �  <     $   @  -   e     �     �  C   �     �     �  !        -           !          
                                                                                                                     "       $             	   #                   ACLs API URL pointing to a permission in relation to the access control list to which it is attached. This URL is different than the canonical workflow URL. API URL pointing to the list of permissions for this access control list. Access control lists Access control lists for: %s Access entries Access entry Available permissions Comma separated list of permission primary keys to grant to this access control list. Delete Delete ACL: %s Disabled permissions are inherited from a parent object. Edit ACLs Grant access Granted permissions Insufficient access for: %s New ACL New access control lists for: %s No such permission: %s None Numeric identifier of the object for which the access will be modified. Object ID Object type Permissions Permissions "%(permissions)s" to role "%(role)s" for "%(object)s" Permissions to grant/revoke to/from the role for the object selected above. Primary key of the new permission to grant to the access control list. Primary keys of the role to which this access control list binds to. Revoke access Role Role "%(role)s" permission's for "%(object)s" Roles Roles whose access will be modified. Type of the object for which the access will be modified. View ACLs Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-08-27 16:32+0000
Last-Translator: Roberto Rosario
Language-Team: Chinese (China) (http://www.transifex.com/rosarior/mayan-edms/language/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
 访问控制列表 指向相关附件访问控制列表权限的API URL，此URL与标准工作流的URL不同。 指向访问控制列表权限的API URL 访问控制列表 %s 的访问控制列表 多个访问入口 访问入口 可用权限 为访问控制列表授权的主键（以逗号分隔） 删除 删除访问控制列表：%s 父级对象取消的授权会影响下级对象。 编辑访问控制列表 授权访问 已分配权限 无权访问：%s 新建访问控制列表 为 %s 创建新访问控制列表 无此权限：%s 无 被修改对象的数字化标识 对象ID 对象类型 权限 为角色 "%(role)s" 分配的权限 "%(permissions)s"，适用于对象 "%(object)s" 对以上所选对象的角色进行取消或授权的权限 为访问控制列表授权的主键 此访问控制列表所绑定角色的主键 取消授权 角色 为角色 "%(role)s" 分配的权限，适用于对象 "%(object)s" 角色 被修改权限的角色 被修改权限的对象的类型 查看访问控制列表 